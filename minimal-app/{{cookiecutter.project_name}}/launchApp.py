#!python
import sys
version = sys.version_info
if version < (3, 5, 3):
    raise SystemExit("python 3.5.3 or higher required")

from {{ cookiecutter.package_name }} import main
