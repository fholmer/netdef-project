from netdef.__main__ import entrypoint
{% if cookiecutter.include_service_entrypoint == 'y' -%}

def service():
    from netdef.service import get_service, run_service
    application_service = get_service("{{ cookiecutter.project_name }}", "{{ cookiecutter.project_name }}-Service", run_app, get_template_config)
    run_service(application_service)
{% endif -%}

def run_app():
    from . import main

def get_template_config():
    from . import defaultconfig
    return defaultconfig.template_config_string

def cli():
    # entrypoint: console_scripts
    entrypoint(run_app, get_template_config)

if __name__ == '__main__':
    # entrypoint: python -m console_scripts 
    entrypoint(run_app, get_template_config)


