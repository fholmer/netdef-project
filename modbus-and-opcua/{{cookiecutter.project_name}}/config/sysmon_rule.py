# - CSVRule looks for the function-name: expression.
# - There will be as many arguments as there is columns in csv-files
# - The arguments is an instance of netdef.Engine.expression.Expression.Argument
def expression(sysmon, opcua, modbus):

    # update on first value and updated values
    if sysmon.new or sysmon.update:

        # opc-ua will figure out the value datatype 
        opcua.set = sysmon.value

        # modbus only support integers
        if isinstance(sysmon.value, int):
            modbus.set = sysmon.value
        elif isinstance(sysmon.value, float):
            modbus.set = int(sysmon.value)
        else:
            print(sysmon.key, type(sysmon.value), "datatype not supported")
