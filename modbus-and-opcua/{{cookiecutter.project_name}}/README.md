{{ cookiecutter.project_name }}
===============================

author: {{ cookiecutter.full_name }}

Overview
--------

{{ cookiecutter.project_short_description }}

Installation / Usage
--------------------

To install use pip:

    $ pip install {{ cookiecutter.project_name }}

To setup workdir:

    $ {{ cookiecutter.project_name }} -i [workdir]

To run application:

    $ {{ cookiecutter.project_name }} -r [workdir]


Example (Deployment)
--------------------

    $ mkdir -p /opt/{{ cookiecutter.project_name }}
    $ cd /opt/{{ cookiecutter.project_name }}
    $ python3 -m venv .
    $ . bin/activate
    $ pip install {{ cookiecutter.project_name }}
    $ {{ cookiecutter.project_name }} -i [workdir]

    # run application in foreground:
    $ {{ cookiecutter.project_name }} -r [workdir]
