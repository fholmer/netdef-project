import os
from netdef.Controllers import Controllers
from netdef.Sources import Sources
from netdef.Rules import Rules
{% if cookiecutter.netdef_webadmin == "built-in" -%}
from netdef.Engines import ThreadedWebGuiEngine
{% elif cookiecutter.netdef_webadmin == "nginx-reverse-proxy" -%}
from netdef.Engines import NginxWebGuiReverseProxy
{% else -%}
from netdef.Engines import ThreadedEngine
{% endif -%}
from netdef.Shared import Shared
from netdef.utils import setup_logging, handle_restart
from . import defaultconfig

def main():
    # init shared-module
    try:
        install_path = os.path.dirname(__file__)
        proj_path = os.getcwd()
        config_string = defaultconfig.default_config_string
        shared = Shared.Shared("{{ cookiecutter.project_name }}", install_path, proj_path, config_string)
    except ValueError as error:
        print(error)
        raise SystemExit(1)

    # configure logging
    setup_logging(shared.config)

    controllers = Controllers.Controllers(shared)
    controllers.load([__package__, 'netdef'])

    sources = Sources.Sources(shared)
    sources.load([__package__, 'netdef'])

    rules = Rules.Rules(shared)
    rules.load([__package__, 'netdef'])

    # the engine connects webadmin, controllers, sources and rules.
    {% if cookiecutter.netdef_webadmin == "built-in" -%}
    engine = ThreadedWebGuiEngine.ThreadedWebGuiEngine(shared)
    {% elif cookiecutter.netdef_webadmin == "nginx-reverse-proxy" -%}
    engine = NginxWebGuiReverseProxy.NginxReverseProxy(shared)
    {% else -%}
    engine = ThreadedEngine.ThreadedEngine(shared)
    {% endif -%}
    engine.add_controller_classes(controllers)
    engine.add_source_classes(sources)
    engine.add_rule_classes(rules)
    engine.load([__package__, 'netdef'])

    engine.init()
    engine.start()
    engine.block() # until ctrl-c or SIG_TERM
    engine.stop()

    # if restart-button in webadmin is pressed:
    handle_restart(shared, engine)

main()
