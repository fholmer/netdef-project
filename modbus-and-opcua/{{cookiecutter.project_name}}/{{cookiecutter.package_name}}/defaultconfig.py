template_config_string = \
"""[general]
identifier = {{ cookiecutter.project_name }}
version = 1

[config]
security_conf = config/security.conf

[sysmon_rule]
csv = config/sysmon_rule.csv
py = config/sysmon_rule.py

[endpoints]
webadmin_host = 0.0.0.0
webadmin_port = 8000
modbus_host = 0.0.0.0
modbus_port = 8020
opcua_endpoint = opc.tcp://0.0.0.0:4841/freeopcua/server/

"""

default_config_string = \
"""[general]
[config]
[ExpressionExecutor]

[webadmin]
host = ${endpoints:webadmin_host}
port = ${endpoints:webadmin_port}

[webadmin_views]

[logging]
logglevel = 20
loggformat = %(asctime)-15s %(levelname)-9s: %(name)-11s: %(message)s
loggdatefmt = %Y-%m-%d %H:%M:%S
to_console = 1
to_file = 1

[logginglevels]
werkzeug = 40
opcua = 40

[controllers]
SystemMonitorController = 1
OPCUAServerController = 1
ModbusServerController = 1

[controller_aliases]

[rules]
CSVRule = 1

[sources]
SystemMonitorSource = 1
VariantSource = 1
HoldingRegisterSource = 1

[source_aliases]

[CSVRule]
sysmon_rule = 1

[SystemMonitorSource]
controller = SystemMonitorController

[VariantSource]
controller = OPCUAServerController

[HoldingRegisterSource]
controller = ModbusServerController

[OPCUAServerController]
anonymous_on = 1
username_on = 0
full_encryption_on = 0
user =${security:opcua_user}
password =${security:opcua_password}
password_hash =${security:opcua_password_hash}
endpoint = ${endpoints:opcua_endpoint}
#certificate = config/controllers/opcua/certificates/my_cert.der
#private_key = config/controllers/opcua/certificates/my_private_key.pem

[ModbusServerController]
host = ${endpoints:modbus_host}
port = ${endpoints:modbus_port}

[ModbusServerController_devices]
ModbusServerController_device0 = 1

[ModbusServerController_device0]

[security]
opcua_user = admin
opcua_password = admin
opcua_password_hash =

[endpoints]
webadmin_host = 0.0.0.0
webadmin_port = 8000
modbus_host = 0.0.0.0
modbus_port = 8020
opcua_endpoint = opc.tcp://0.0.0.0:4841/freeopcua/server/

"""
